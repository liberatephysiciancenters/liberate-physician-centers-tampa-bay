Liberate Physician Centers of Tampa Bay is dedicated to providing customized cannabis programs for members of our community who have been diagnosed with any of the State of Florida approved conditions.

Address: 14414 N Florida Ave, Tampa, FL 33613, USA

Phone: 813-567-3200

Website: https://liberatetampabay.com
